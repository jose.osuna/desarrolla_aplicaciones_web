**Programación**
5AVP
Osuna Quintero José Antonio

- Práctica #1 - 02/09/2022 - Práctica de ejemplo
Commit: e9f6654e5d132c9862319d67f37274a62dc9eca2
Archivo: https://gitlab.com/jose.osuna/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Práctica #2 - 09/09/2022 - Práctica JavaScript
Commit: cf85dcb695c86e72dbbb4870c8f33a0a92afe614
Archivo: https://gitlab.com/jose.osuna/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaJavaScript.html

- Práctica #3 - 15/09/2022 - Práctica Bootstrap - Práctica web con base de datos
Commit: 27805fa79947d84aa8796cda5c45af8ddbda260c
Archivo: https://gitlab.com/jose.osuna/desarrolla_aplicaciones_web/-/blob/main/parcial1/bootstrap-4.6.2-dist__1_.zip

- Práctica #4 - 19/09/2022 - Práctica web con base de datos - Vista de consulta de datos
Commit: dae26290b4af396a636b4652785ee3b5e57a9e88

- Práctica #5 - 19/09/2022 - Práctica web con base de datos - Vista de consulta de datos
Commit: c607d0b05e7c86cc63ac4859ea92bb1726136af8
